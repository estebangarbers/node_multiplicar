const { crearArchivo, listarTabla } = require('./multiplicar/multiplicar');
const argv = require('./config/yargs').argv;
const colors = require('colors/safe');


let comando = argv._[0];

switch (comando) {
    case 'listar':
        listarTabla(argv.base, argv.limite);
        break;
    case 'crear':
        crearArchivo(argv.base, argv.limite)
            .then(archivo => console.log(`Archivo creado:`, colors.rainbow(archivo)))
            .catch(err => console.log(err));
        break;
    case 'eliminar':
        console.log('Eliminar');
        break;
    default:
        console.log('Comando no reconocido');
}
// let base = 'abc';

//let argv2 = process.argv;

//console.log(argv);

// let parametro = argv[2];
// let base = parametro.split('=')[1];
//console.log(base);