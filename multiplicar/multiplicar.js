// requireds
const fs = require('fs');
const colors = require('colors/safe');
//module.exports.crearArchivo

let listarTabla = (base, limite = 10) => {
    console.log('============================'.green);
    console.log(`========Tabla de ${base}========`.green);
    console.log('============================'.green);
    for (var i = 1; i <= limite; i++)
        console.log(`${base} * ${i} = ${base*i}`);
}

let crearArchivo = (base, limite = 10) => {
    return new Promise((resolve, reject) => {

        if (!Number(base)) {
            reject(`La base ${base} no es un numero`);
            return;
        }

        if (!Number(limite) || limite <= 0) {
            reject(`El limite debe ser un numero mayor a 0`);
            return;
        }

        let data = '';
        for (var i = 1; i <= limite; i++)
            data += `${base} * ${i} = ${base*i}\n`;

        fs.writeFile(`tablas/tabla-${base}.txt`, data, (err) => {
            if (err) reject(err);
            else resolve(`tabla-${base}.txt`);
            console.log('The file has been saved!'.bgRed);
        });
    });
}

module.exports = {
    crearArchivo,
    listarTabla
}